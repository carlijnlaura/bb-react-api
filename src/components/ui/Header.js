import React from 'react'
import logo from '../../img/logo.png'
import walter from '../../img/walter.png'

const Header = () => {
    return (
        <header className='center'>
            <img src={walter} style={{ margin:'20px', marginTop: '30px', height: '200px', width: '150px'}} alt='Walter' />
            <img src={logo} alt='logo' />
        </header>
    )
}

export default Header
