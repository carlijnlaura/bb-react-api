import React from 'react'

const SubItem = ({ item }) => {
    return (
        <div className='card'>
        <div className='card-inner'>
          <div className='card-front'>
            <img src={item.img} alt='' />
          </div>
          <div className='card-back'>
            <h1>{item.name}</h1>
            <ul>
              <li>
                <strong>Category:</strong> {item.category}
              </li>
              <li>
                <strong>Occupation:</strong> {item.occupation}
              </li>
            </ul>
          </div>
        </div>
      </div>
    )
}

export default SubItem
