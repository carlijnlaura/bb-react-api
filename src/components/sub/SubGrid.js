import React from 'react'
import SubItem from './SubItem'
import Spinner from '../ui/Spinner'

const SubGrid = ({ items, isLoading }) => {
    return (
        isLoading ? (<Spinner/>) : (
        <section className='cards'>
            {items.map((item) => (
                <SubItem key={item.char_id} item={item}></SubItem>
            ))}
        </section>)
    )
}

export default SubGrid