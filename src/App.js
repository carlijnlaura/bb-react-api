import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css'

import Header from './components/ui/Header'
import Navbar from './components/Navbar'
import Search from './components/ui/Search'

import CharacterGrid from './components/characters/CharacterGrid'
import SubGrid from './components/sub/SubGrid'
import Pagination from './components/Pagination'




const App = () => {
  const [items, setItems] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [currentPage, setCurrentPage] = useState(1)
  const [itemsPerPage] = useState(8)
  const [query, setQuery] = useState('')


  useEffect(() => {
    const fetchItems = async () => {
      const result = await axios(`https://www.breakingbadapi.com/api/characters?name=${query}`)
    
      setItems(result.data)
      setIsLoading(false)
  
    }
    fetchItems()
  }, [query])

  //Get current items
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = items.slice(indexOfFirstItem, indexOfLastItem);

  // Change page
  const paginate = pageNumber => setCurrentPage(pageNumber);



  return (
    <Router>
      <div className="container">
        <Header/>
        <Navbar/>

        <Switch>
          <Route exact path="/sub">
            <p>- Sub information -</p>
              <Search getQuery={(q) => setQuery(q)}/>
              <SubGrid isLoading={isLoading} items={currentItems} />
              <Pagination itemsPerPage={itemsPerPage} totalItems={items.length} paginate={paginate}/>
          </Route>

           <Route path="/">
             <p>- Home -</p>
            <Search getQuery={(q) => setQuery(q)}/>
            <CharacterGrid isLoading={isLoading} items={currentItems} />
            <Pagination itemsPerPage={itemsPerPage} totalItems={items.length} paginate={paginate}/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
